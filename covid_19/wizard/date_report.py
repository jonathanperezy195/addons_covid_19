# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import UserError

class DateReportWizard(models.TransientModel):
    _name = 'covid.date.report.wizard'
    _description='Report betwenn date and by country'

    start_date=fields.Date("Start Date",required=True)
    end_date=fields.Date("End Date",required=True)
    country_ids=fields.Many2many(
        "res.country", 
        string="Countries", 
        help="Countries you want to generate the report"
    )

    def print_report(self):
        print("Probando")
        return True
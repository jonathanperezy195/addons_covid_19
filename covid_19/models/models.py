# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Covid_19(models.Model):
    _name = 'covid.covid_19'
    _order='id desc'
    _description='Covid 19 modelo'

    source = fields.Char(string="Source",required=True)
    date = fields.Datetime(string="Date Time",required=True, default=fields.Datetime.now())
    country_id = fields.Many2one('res.country',required=True,default=0)
    infected = fields.Integer(string="Infected",required=True,default=0)
    recovered = fields.Integer(string="Recovered",required=True,default=0)
    deceased = fields.Integer(string="Deceased",required=True,default=0)
    total_infecteds = fields.Integer(string="Total Infecteds",compute="set_total_infecteds")
    total_recovereds = fields.Integer(string="Total recovereds",compute="set_total_recovereds")
    total_deceaseds = fields.Integer(string="Total deceaseds",compute="set_total_deceaseds")

    def set_total_infecteds(self):
        for data in self:
            data.total_infecteds=sum(self.get_records_map(data,"infected")) + data.infected
    
    def set_total_recovereds(self):
        for data in self:
            data.total_recovereds=sum(self.get_records_map(data,"recovered")) + data.recovered
    
    def set_total_deceaseds(self):
        for data in self:
            data.total_deceaseds=sum(self.get_records_map(data,"deceased")) + data.deceased 
        
    # obtener el mapa
    def get_records_map(self,data,propiedad):
        records = self.search(self.get_dom_base(data))
        return records.mapped(propiedad)

    # Obtener el tipo de busqueda basico
    def get_dom_base(self,data):
        return [
            ('country_id','=',data.country_id.id),
            ('date','<',data.date),
        ]

    # generar el porcentaje de infectados
    def set_avg_infected(self):
        return self.get_avg(self.infected,self.total_infecteds)
    
    # generar el porcentaje de recuperados
    def set_avg_recovered(self):
        return self.get_avg(self.recovered,self.total_recovereds)
    
    # generar el porcentaje de fallecidos
    def set_avg_deceased(self):
        return self.get_avg(self.deceased,self.total_deceaseds)

    # validar y calcular promedio
    def get_avg(self,prop,totals):
        total=0
        if prop > 0:
            total = round((prop/totals) * 100, 2)
        return total

#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100